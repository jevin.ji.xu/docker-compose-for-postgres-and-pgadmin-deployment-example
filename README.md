The stack compose 2 service:
- postgres as DB.
- pgadmin as DBA tool to manage postgres DB.

Depoly whole stack in docker env:

- mkdir -p /opt/database/pgdata
- mkdir -p /opt/database/pgadmin

docker stack deploy -c docker-compose.yml databaseSuite

Then open https://EXPOSTURL to access pgadmin.